app = angular.module 'sykora', ['ui.bootstrap']

app.config ($interpolateProvider) ->
    $interpolateProvider.startSymbol '[['
    $interpolateProvider.endSymbol ']]'


app.controller 'Config', ($scope, $window, $http) ->
    Math = $window.Math
    $scope.config = {
        preview: false
        # rowCount: 22
        # colCount: 11
        rowCount: 5 
        colCount: 5 
        shapes: [
            {density:1, rotation:'z', ctop: 1, cright: 1, cbottom: 1, cleft: 0, stop: 0, sright: 0, sbottom: 0, sleft: 1}
            {density:1, rotation:'y', ctop: 1, cright: 0, cbottom: 1, cleft: 1, stop: 0, sright: 1, sbottom: 0, sleft: 0}
            {density:1, rotation:'r', ctop: 1, cright: 0, cbottom: 1, cleft: 0, stop: 0, sright: 1, sbottom: 0, sleft: 1}
            {density:1, rotation:'i', ctop: 0, cright: 1, cbottom: 1, cleft: 1, stop: 1, sright: 0, sbottom: 0, sleft: 0}
            {density:1, rotation:'d', ctop: 0, cright: 1, cbottom: 0, cleft: 1, stop: 1, sright: 0, sbottom: 1, sleft: 0}
            {density:1, rotation:'b', ctop: 1, cright: 1, cbottom: 0, cleft: 1, stop: 0, sright: 0, sbottom: 1, sleft: 0}

            {density:2, rotation:'b', ctop: 0, cright: 0, cbottom: 1, cleft: 0, stop: 0, sright: 0, sbottom: 1, sleft: 0}
            {density:2, rotation:'r', ctop: 1, cright: 0, cbottom: 0, cleft: 0, stop: 1, sright: 0, sbottom: 0, sleft: 0}
            {density:2, rotation:'y', ctop: 0, cright: 1, cbottom: 0, cleft: 0, stop: 0, sright: 1, sbottom: 0, sleft: 0}
            {density:2, rotation:'z', ctop: 0, cright: 0, cbottom: 0, cleft: 1, stop: 0, sright: 0, sbottom: 0, sleft: 1}

            {density:3, rotation:'b', ctop: 1, cright: 1, cbottom: 0, cleft: 1, stop: 0, sright: 0, sbottom: 1, sleft: 0}
            {density:3, rotation:'r', ctop: 0, cright: 1, cbottom: 1, cleft: 1, stop: 1, sright: 0, sbottom: 0, sleft: 0}
            {density:3, rotation:'y', ctop: 1, cright: 0, cbottom: 1, cleft: 1, stop: 0, sright: 1, sbottom: 0, sleft: 0}
            {density:3, rotation:'z', ctop: 1, cright: 1, cbottom: 1, cleft: 0, stop: 0, sright: 0, sbottom: 0, sleft: 1}

            {density:4, rotation:'b', ctop: 0, cright: 0, cbottom: 1, cleft: 0, stop: 0, sright: 0, sbottom: 1, sleft: 0}
            {density:4, rotation:'d', ctop: 1, cright: 0, cbottom: 1, cleft: 0, stop: 1, sright: 0, sbottom: 1, sleft: 0}
            {density:4, rotation:'i', ctop: 1, cright: 0, cbottom: 0, cleft: 0, stop: 1, sright: 0, sbottom: 0, sleft: 0}
            {density:4, rotation:'r', ctop: 0, cright: 1, cbottom: 0, cleft: 1, stop: 0, sright: 1, sbottom: 0, sleft: 1}
            {density:4, rotation:'y', ctop: 0, cright: 1, cbottom: 0, cleft: 0, stop: 0, sright: 1, sbottom: 0, sleft: 0}
            {density:4, rotation:'z', ctop: 0, cright: 0, cbottom: 0, cleft: 1, stop: 0, sright: 0, sbottom: 0, sleft: 1}
        ]
        rule: 0
        rules: [
            {id: 0, name: '+barvy, +tvary'},
            {id: 1, name: '+barvy, -tvary'},
            {id: 2, name: '-barvy, +tvary'},
            {id: 3, name: '-barvy, -tvary'}
        ]
        const: 0.75
        fix: {}
        out: {}
    }

    $scope.outCalculate = ->
        $scope.outClear()

        for row in [0..$scope.config.rowCount-1]
            for colIndex in [0..$scope.config.colCount-1]
                if row % 2 == 0
                    col = colIndex
                else
                    col = $scope.config.colCount - colIndex - 1

                # square is fixed by author
                if $scope.config.fix[[row,col]]?.shape
                    $scope.config.out[[row,col]] = $scope.config.fix[[row,col]]
                else
                    candidateShapes = angular.copy($scope.config.shapes)
                    candidateShapes = filterDensity(candidateShapes, row, col)
                    
                    candidateShapes = filterNeighbors(candidateShapes, row, col, 'c', $scope.config.rule==0 or $scope.config.rule==1)
                    candidateShapes = filterNeighbors(candidateShapes, row, col, 's', $scope.config.rule==0 or $scope.config.rule==2)

                    shape = candidateShapes[Math.floor(Math.random() * candidateShapes.length)] 
                    $scope.config.out[[row, col]] = {shape: shape}

    $scope.save = ->
        $scope.config.start = angular.copy($scope.config.out)
        $scope.outCalculate()
        $scope.config.end = angular.copy($scope.config.out)
        $http.post('/save/', $scope.config).success (data) ->
            $scope.savedId = data.id

    $scope.load = ->
        $http.get('/load/' + $scope.loadId + '/').success (data) ->
            $scope.config = data
            $scope.savedId = $scope.loadId

    # get shape on position x, y
    # prefer set by author, else get one already calculated, otherwise null
    calculatedShape = (row, col) ->
        return $scope.config.fix[[row, col]]?.shape or $scope.config.out[[row, col]]?.shape or null

    diff = (row, col) ->
        return ($scope.config.fix[[row, col]]?.diff or 0) * $scope.config.const

    filterNeighbors = (candidateShapes, row, col, prefix, shouldContinue) ->
        AROUND = [
            [-1, +0, 'bottom', 'top']
            [+1, +0, 'top', 'bottom']
            [+0, -1, 'right', 'left']
            [+0, +1, 'left', 'right']
        ]
        for [dRow, dCol, neighborAttr, candidateAttr] in AROUND
            neighbor = calculatedShape(row+dRow, col+dCol)
            if neighbor 
                filteredShapes = candidateShapes.filter((shape) -> (neighbor[prefix+neighborAttr] == shape[prefix+candidateAttr])==shouldContinue)
                if filteredShapes.length > 0 then candidateShapes = filteredShapes
        return candidateShapes

    filterDensity = (candidateShapes, row, col, shouldContinue) ->
        AROUND = [
            [-1, -1]
            [+0, -1]
            [+1, -1]
            [-1, +0]
            [+1, +0]
            [-1, +1]
            [-0, +1]
            [+1, +1]
        ]
        count = 0
        sum = 0 
        for [dRow, dCol] in AROUND
            shape = calculatedShape(row+dRow, col+dCol)
            if shape
                count += 1
                sum += shape.density

        if count
            density = sum / count
        else
            density = 1

        density += diff(row, col)
        density = Math.min(4, Math.max(1, Math.round(density)))
        filteredShapes = candidateShapes.filter((shape)-> shape.density==density)

        if filteredShapes.length > 0
            return filteredShapes
        else
            return candidateShapes

    $scope.outClear = ->
        for ii in [0..$scope.config.rowCount-1]
            for jj in [0..$scope.config.colCount-1]
                delete $scope.config.out[[ii,jj]]

    $scope.$watch '[config.rowCount,config.colCount]', (->
        $scope.rows = [0..$scope.config.rowCount-1]
        $scope.cols = [0..$scope.config.colCount-1]
    ), true

    $scope.$watch '[config.const,config.rule,config.preview]', $scope.outCalculate, true

app.controller 'Square', ($scope) ->
    $scope.updateImage = ->
        config = $scope.config

        if config.preview
            data = $scope.config.out[[$scope.row,$scope.col]] 
        else
            data = $scope.config.fix[[$scope.row,$scope.col]] 
        
        if data?.shape
            $scope.img = data.shape.density + data.shape.rotation
        else if data?.diff
            $scope.img = if data.diff > 0 then 'plus' else 'minus'
        else
            $scope.img = 'empty'

        
    $scope.$watch '[config.preview,config.out[['+$scope.row+','+$scope.col+']],config.fix[['+$scope.row+','+$scope.col+']]]', $scope.updateImage, true

    $scope.shapeFix = (shape) ->
        $scope.config.fix[[$scope.row, $scope.col]] = {shape: shape}
        $scope.outCalculate()
    
    $scope.shapeDiff = (diff) ->
        $scope.config.fix[[$scope.row, $scope.col]] = {diff: diff}
        $scope.outCalculate()

    $scope.shapeClear = ->
        delete $scope.config.fix[[$scope.row, $scope.col]]
        $scope.outCalculate()
