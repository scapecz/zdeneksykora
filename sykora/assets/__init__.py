from django_assets import Bundle, register

register(
    'scripts',
    Bundle(
        'scripts/main.coffee',

        filters='coffeescript',
        output='../static/generated/all.js'
    )
)

register(
    'styles',
    Bundle('styles/main.sass', filters='compass'),

    filters='compass',
    output='../static/generated/all.css'
)

