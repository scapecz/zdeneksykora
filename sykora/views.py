from os import path, makedirs
import zipfile
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.conf import settings
from PIL import Image as PilImage

from models import Image

@csrf_exempt
def save(request):
    # config = simplejson.loads(request.body)
    image = Image(config=request.body)
    image.save()

    data = {'id': image.id}

    return HttpResponse(simplejson.dumps(data))

def load(request, id):
    image = get_object_or_404(Image, id=id)
    return HttpResponse(image.config)


def get_animation(request, id):
    image = get_object_or_404(Image, id=id)
    config = simplejson.loads(image.config)
    # print config
    # for i in config['start']:
    # print config['start']keys()
    # print config['start'].values()
    animation_zip_name = 'animations/%s.zip' % image.id 
    animation_zip_path = path.join(settings.MEDIA_ROOT, animation_zip_name) 

    if not path.exists(animation_zip_path):
        animation_zip = zipfile.ZipFile(animation_zip_path, 'w')
        animation_dir = path.join(settings.MEDIA_ROOT, 'animations', '%s' % image.id)
        makedirs(animation_dir)

        counter = 0
        
        fname = path.join(animation_dir, '%04d.png' % counter) 
        create_image(config, fname)
        animation_zip.write(fname, arcname='%04d.png' % counter)
        
        for row in range(config['rowCount']):
            for colIndex in range(config['colCount']):
                if row % 2 == 0:
                    col = colIndex
                else:
                    col = config['colCount'] - colIndex - 1

                counter += 1
                config['start']['%s,%s' % (row, col)]['shape'] = config['end']['%s,%s' % (row, col)]['shape']
                
                fname = path.join(animation_dir, '%04d.png' % counter) 
                create_image(config, fname)
                animation_zip.write(fname, arcname='%04d.png' % counter)

                # shape1 = config['start']['%s,%s' % (row, col)]['shape']
                # shape2 = config['end']['%s,%s' % (row, col)]['shape']
        #         print shape1['density'], shape1['rotation'], shape2['density'], shape2['rotation']
        animation_zip.close()
    return HttpResponseRedirect(settings.MEDIA_URL + animation_zip_name)

IMG_WIDTH = 50
IMG_HEIGHT = 50
def create_image(config, filename):
    out_path = './test.svg'
    out = PilImage.new('RGB', (config['colCount'] * IMG_WIDTH, config['rowCount'] * IMG_HEIGHT))

    for row in range(config['rowCount']):
        for col in range(config['colCount']):
            shape = config['start']['%s,%s' % (row, col)]['shape'] 
            shapeImg = PilImage.open(path.join(settings.STATIC_ROOT, 'img/circle/%s%s.png' % (shape['density'], shape['rotation'])))
            out.paste(shapeImg, (col*IMG_WIDTH, row*IMG_HEIGHT))

            # print shapeImg, row*IMG_HEIGHT, col*IMG_WIDTH
    # out.show()
    out.save(filename)

            # png.paste()