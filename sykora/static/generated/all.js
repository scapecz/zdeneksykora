var app;

app = angular.module('sykora', ['ui.bootstrap']);

app.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  return $interpolateProvider.endSymbol(']]');
});

app.controller('Config', function($scope, $window, $http) {
  var Math, calculatedShape, diff, filterDensity, filterNeighbors;
  Math = $window.Math;
  $scope.config = {
    preview: false,
    rowCount: 5,
    colCount: 5,
    shapes: [
      {
        density: 1,
        rotation: 'z',
        ctop: 1,
        cright: 1,
        cbottom: 1,
        cleft: 0,
        stop: 0,
        sright: 0,
        sbottom: 0,
        sleft: 1
      }, {
        density: 1,
        rotation: 'y',
        ctop: 1,
        cright: 0,
        cbottom: 1,
        cleft: 1,
        stop: 0,
        sright: 1,
        sbottom: 0,
        sleft: 0
      }, {
        density: 1,
        rotation: 'r',
        ctop: 1,
        cright: 0,
        cbottom: 1,
        cleft: 0,
        stop: 0,
        sright: 1,
        sbottom: 0,
        sleft: 1
      }, {
        density: 1,
        rotation: 'i',
        ctop: 0,
        cright: 1,
        cbottom: 1,
        cleft: 1,
        stop: 1,
        sright: 0,
        sbottom: 0,
        sleft: 0
      }, {
        density: 1,
        rotation: 'd',
        ctop: 0,
        cright: 1,
        cbottom: 0,
        cleft: 1,
        stop: 1,
        sright: 0,
        sbottom: 1,
        sleft: 0
      }, {
        density: 1,
        rotation: 'b',
        ctop: 1,
        cright: 1,
        cbottom: 0,
        cleft: 1,
        stop: 0,
        sright: 0,
        sbottom: 1,
        sleft: 0
      }, {
        density: 2,
        rotation: 'b',
        ctop: 0,
        cright: 0,
        cbottom: 1,
        cleft: 0,
        stop: 0,
        sright: 0,
        sbottom: 1,
        sleft: 0
      }, {
        density: 2,
        rotation: 'r',
        ctop: 1,
        cright: 0,
        cbottom: 0,
        cleft: 0,
        stop: 1,
        sright: 0,
        sbottom: 0,
        sleft: 0
      }, {
        density: 2,
        rotation: 'y',
        ctop: 0,
        cright: 1,
        cbottom: 0,
        cleft: 0,
        stop: 0,
        sright: 1,
        sbottom: 0,
        sleft: 0
      }, {
        density: 2,
        rotation: 'z',
        ctop: 0,
        cright: 0,
        cbottom: 0,
        cleft: 1,
        stop: 0,
        sright: 0,
        sbottom: 0,
        sleft: 1
      }, {
        density: 3,
        rotation: 'b',
        ctop: 1,
        cright: 1,
        cbottom: 0,
        cleft: 1,
        stop: 0,
        sright: 0,
        sbottom: 1,
        sleft: 0
      }, {
        density: 3,
        rotation: 'r',
        ctop: 0,
        cright: 1,
        cbottom: 1,
        cleft: 1,
        stop: 1,
        sright: 0,
        sbottom: 0,
        sleft: 0
      }, {
        density: 3,
        rotation: 'y',
        ctop: 1,
        cright: 0,
        cbottom: 1,
        cleft: 1,
        stop: 0,
        sright: 1,
        sbottom: 0,
        sleft: 0
      }, {
        density: 3,
        rotation: 'z',
        ctop: 1,
        cright: 1,
        cbottom: 1,
        cleft: 0,
        stop: 0,
        sright: 0,
        sbottom: 0,
        sleft: 1
      }, {
        density: 4,
        rotation: 'b',
        ctop: 0,
        cright: 0,
        cbottom: 1,
        cleft: 0,
        stop: 0,
        sright: 0,
        sbottom: 1,
        sleft: 0
      }, {
        density: 4,
        rotation: 'd',
        ctop: 1,
        cright: 0,
        cbottom: 1,
        cleft: 0,
        stop: 1,
        sright: 0,
        sbottom: 1,
        sleft: 0
      }, {
        density: 4,
        rotation: 'i',
        ctop: 1,
        cright: 0,
        cbottom: 0,
        cleft: 0,
        stop: 1,
        sright: 0,
        sbottom: 0,
        sleft: 0
      }, {
        density: 4,
        rotation: 'r',
        ctop: 0,
        cright: 1,
        cbottom: 0,
        cleft: 1,
        stop: 0,
        sright: 1,
        sbottom: 0,
        sleft: 1
      }, {
        density: 4,
        rotation: 'y',
        ctop: 0,
        cright: 1,
        cbottom: 0,
        cleft: 0,
        stop: 0,
        sright: 1,
        sbottom: 0,
        sleft: 0
      }, {
        density: 4,
        rotation: 'z',
        ctop: 0,
        cright: 0,
        cbottom: 0,
        cleft: 1,
        stop: 0,
        sright: 0,
        sbottom: 0,
        sleft: 1
      }
    ],
    rule: 0,
    rules: [
      {
        id: 0,
        name: '+barvy, +tvary'
      }, {
        id: 1,
        name: '+barvy, -tvary'
      }, {
        id: 2,
        name: '-barvy, +tvary'
      }, {
        id: 3,
        name: '-barvy, -tvary'
      }
    ],
    "const": 0.75,
    fix: {},
    out: {}
  };
  $scope.outCalculate = function() {
    var candidateShapes, col, colIndex, row, shape, _i, _ref, _results;
    $scope.outClear();
    _results = [];
    for (row = _i = 0, _ref = $scope.config.rowCount - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; row = 0 <= _ref ? ++_i : --_i) {
      _results.push((function() {
        var _j, _ref1, _ref2, _results1;
        _results1 = [];
        for (colIndex = _j = 0, _ref1 = $scope.config.colCount - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; colIndex = 0 <= _ref1 ? ++_j : --_j) {
          if (row % 2 === 0) {
            col = colIndex;
          } else {
            col = $scope.config.colCount - colIndex - 1;
          }
          if ((_ref2 = $scope.config.fix[[row, col]]) != null ? _ref2.shape : void 0) {
            _results1.push($scope.config.out[[row, col]] = $scope.config.fix[[row, col]]);
          } else {
            candidateShapes = angular.copy($scope.config.shapes);
            candidateShapes = filterDensity(candidateShapes, row, col);
            candidateShapes = filterNeighbors(candidateShapes, row, col, 'c', $scope.config.rule === 0 || $scope.config.rule === 1);
            candidateShapes = filterNeighbors(candidateShapes, row, col, 's', $scope.config.rule === 0 || $scope.config.rule === 2);
            shape = candidateShapes[Math.floor(Math.random() * candidateShapes.length)];
            _results1.push($scope.config.out[[row, col]] = {
              shape: shape
            });
          }
        }
        return _results1;
      })());
    }
    return _results;
  };
  $scope.save = function() {
    $scope.config.start = angular.copy($scope.config.out);
    $scope.outCalculate();
    $scope.config.end = angular.copy($scope.config.out);
    return $http.post('/save/', $scope.config).success(function(data) {
      return $scope.savedId = data.id;
    });
  };
  $scope.load = function() {
    return $http.get('/load/' + $scope.loadId + '/').success(function(data) {
      $scope.config = data;
      return $scope.savedId = $scope.loadId;
    });
  };
  calculatedShape = function(row, col) {
    var _ref, _ref1;
    return ((_ref = $scope.config.fix[[row, col]]) != null ? _ref.shape : void 0) || ((_ref1 = $scope.config.out[[row, col]]) != null ? _ref1.shape : void 0) || null;
  };
  diff = function(row, col) {
    var _ref;
    return (((_ref = $scope.config.fix[[row, col]]) != null ? _ref.diff : void 0) || 0) * $scope.config["const"];
  };
  filterNeighbors = function(candidateShapes, row, col, prefix, shouldContinue) {
    var AROUND, candidateAttr, dCol, dRow, filteredShapes, neighbor, neighborAttr, _i, _len, _ref;
    AROUND = [[-1, +0, 'bottom', 'top'], [+1, +0, 'top', 'bottom'], [+0, -1, 'right', 'left'], [+0, +1, 'left', 'right']];
    for (_i = 0, _len = AROUND.length; _i < _len; _i++) {
      _ref = AROUND[_i], dRow = _ref[0], dCol = _ref[1], neighborAttr = _ref[2], candidateAttr = _ref[3];
      neighbor = calculatedShape(row + dRow, col + dCol);
      if (neighbor) {
        filteredShapes = candidateShapes.filter(function(shape) {
          return (neighbor[prefix + neighborAttr] === shape[prefix + candidateAttr]) === shouldContinue;
        });
        if (filteredShapes.length > 0) {
          candidateShapes = filteredShapes;
        }
      }
    }
    return candidateShapes;
  };
  filterDensity = function(candidateShapes, row, col, shouldContinue) {
    var AROUND, count, dCol, dRow, density, filteredShapes, shape, sum, _i, _len, _ref;
    AROUND = [[-1, -1], [+0, -1], [+1, -1], [-1, +0], [+1, +0], [-1, +1], [-0, +1], [+1, +1]];
    count = 0;
    sum = 0;
    for (_i = 0, _len = AROUND.length; _i < _len; _i++) {
      _ref = AROUND[_i], dRow = _ref[0], dCol = _ref[1];
      shape = calculatedShape(row + dRow, col + dCol);
      if (shape) {
        count += 1;
        sum += shape.density;
      }
    }
    if (count) {
      density = sum / count;
    } else {
      density = 1;
    }
    density += diff(row, col);
    density = Math.min(4, Math.max(1, Math.round(density)));
    filteredShapes = candidateShapes.filter(function(shape) {
      return shape.density === density;
    });
    if (filteredShapes.length > 0) {
      return filteredShapes;
    } else {
      return candidateShapes;
    }
  };
  $scope.outClear = function() {
    var ii, jj, _i, _ref, _results;
    _results = [];
    for (ii = _i = 0, _ref = $scope.config.rowCount - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; ii = 0 <= _ref ? ++_i : --_i) {
      _results.push((function() {
        var _j, _ref1, _results1;
        _results1 = [];
        for (jj = _j = 0, _ref1 = $scope.config.colCount - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; jj = 0 <= _ref1 ? ++_j : --_j) {
          _results1.push(delete $scope.config.out[[ii, jj]]);
        }
        return _results1;
      })());
    }
    return _results;
  };
  $scope.$watch('[config.rowCount,config.colCount]', (function() {
    var _i, _j, _ref, _ref1, _results, _results1;
    $scope.rows = (function() {
      _results = [];
      for (var _i = 0, _ref = $scope.config.rowCount - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; 0 <= _ref ? _i++ : _i--){ _results.push(_i); }
      return _results;
    }).apply(this);
    return $scope.cols = (function() {
      _results1 = [];
      for (var _j = 0, _ref1 = $scope.config.colCount - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; 0 <= _ref1 ? _j++ : _j--){ _results1.push(_j); }
      return _results1;
    }).apply(this);
  }), true);
  return $scope.$watch('[config.const,config.rule,config.preview]', $scope.outCalculate, true);
});

app.controller('Square', function($scope) {
  $scope.updateImage = function() {
    var config, data;
    config = $scope.config;
    if (config.preview) {
      data = $scope.config.out[[$scope.row, $scope.col]];
    } else {
      data = $scope.config.fix[[$scope.row, $scope.col]];
    }
    if (data != null ? data.shape : void 0) {
      return $scope.img = data.shape.density + data.shape.rotation;
    } else if (data != null ? data.diff : void 0) {
      return $scope.img = data.diff > 0 ? 'plus' : 'minus';
    } else {
      return $scope.img = 'empty';
    }
  };
  $scope.$watch('[config.preview,config.out[[' + $scope.row + ',' + $scope.col + ']],config.fix[[' + $scope.row + ',' + $scope.col + ']]]', $scope.updateImage, true);
  $scope.shapeFix = function(shape) {
    $scope.config.fix[[$scope.row, $scope.col]] = {
      shape: shape
    };
    return $scope.outCalculate();
  };
  $scope.shapeDiff = function(diff) {
    $scope.config.fix[[$scope.row, $scope.col]] = {
      diff: diff
    };
    return $scope.outCalculate();
  };
  return $scope.shapeClear = function() {
    delete $scope.config.fix[[$scope.row, $scope.col]];
    return $scope.outCalculate();
  };
});
