# -*- coding: utf-8 -*-
from os import path
from datetime import datetime
from fabric.api import run as fabric_run, local as local_run, cd, put, env
from fabric.decorators import hosts
from fabric.operations import prompt
from fabric.contrib.files import append, exists as fabric_exists
from fabric.contrib.console import confirm

try:
    from carollinum import settings
except ImportError, e:
    settings = None

env.username = path.dirname(__file__).split('/')[-1] 
env.new_pip_version = True
# Environment configurations
def local():
    env.host = None
    env.host_string = None
    env.root_directory = path.dirname(__file__)
    # env.project_directory = path.join(env.root_directory, 'project')
    env.virtualenv_directory = path.join(env.root_directory, 'env')
    env.virtualenv_activate = '. %s/bin/activate' % env.virtualenv_directory
    env.locale_directory = path.join(env.root_directory, 'data/locale')
local() # default is run local

def server(): # abstract
    env.user_directory = path.join('/home/', env.username)
    env.root_directory = path.join(env.user_directory, 'www')
    # env.project_directory = env.root_directory + '/project'
    env.apache_directory = env.root_directory + '/apache'
    env.virtualenv_directory = env.root_directory + '/env'
    env.virtualenv_activate = 'source %s/bin/activate' % env.virtualenv_directory
    env.locale_directory = path.join(env.root_directory, 'data/locale')

def dev():
    server()
    env.host = 'dev.s-cape.cz' 
    env.host_string = '%s@%s' % (env.username, env.host)

def production(force=False):
    if force or confirm('Are you sure you want to work on *PRODUCTION* server?', default=False):
        server()
        env.host = 'production.s-cape.cz' 
        env.host_string = '%s@%s' % (env.username, env.host)
    else:
        abort('User canceled')

# Actual commands
def deploy(*args):
    vc.up()
    check_virtualenv()
    bower_install()
    check_requirements(upgrade='upgrade' in args)
    syncdb()
    migrate_db()
    if env.host:
        collectstatic()
        touch_wsgi()
    # makemessages()

def check_virtualenv():
    if _exists(env.virtualenv_directory):
        print 'Virtualenv directory already exists'
    else:
        print 'Creating virtualenv ...'
        create_virtualenv()

def create_virtualenv():
    _run('virtualenv %s' % env.virtualenv_directory)

    # if env.new_pip_version:
    #     _run('virtualenv %s --system-site-packages' % env.virtualenv_directory)
    # else:
    #     _run('virtualenv %s' % env.virtualenv_directory)
 
def check_requirements(upgrade=False):
    print 'Checking requirements ...'

    for python_requirements in ['%s/requirements.txt' % env.root_directory, '%s/requirements/python.txt' % env.root_directory]:
        if _exists(python_requirements):
            if env.new_pip_version:
                _virtualenv_run('pip install --requirement %s %s' % (python_requirements, ('--upgrade' if upgrade else '')))
            else:
                _run('pip install --requirement %s -E %s' % (python_requirements, env.virtualenv_directory))

    node_requirements = '%s/requirements/node.json' % env.root_directory
    if not env.host and _exists(node_requirements):
        _run('cp %s %s/package.json' % (node_requirements, env.root_directory))
        _run('cd %s && npm install' % env.root_directory)
        _run('rm %s/package.json' % env.root_directory)

def syncdb():
    print 'Synchronizing database'
    _run('cd %s && ./run syncdb' % env.root_directory)

def collectstatic():
    if env.host:
        print 'Collecting static files'
        _run('cd %s && ./run collectstatic --noinput' % env.root_directory)

def migrate_db():
    print 'Running south migrations'
    _run('cd %s && ./run migrate --noinput' % env.root_directory)

def makemessages():
    print 'Processing translations'
    if not _exists(env.locale_directory):
        _run('mkdir %s' % env.locale_directory)
    tmp_directory = path.join(env.root_directory, 'locale')
    _run('ln -s %s %s' % (env.locale_directory, tmp_directory))
    for lang, name in settings.LANGUAGES:
        _run('cd %s; ./run makemessages -l %s --ignore="env/*"' % (env.root_directory, lang)) 
    _run('rm %s' % tmp_directory)

def bower_install():
    print 'Bower install'
    _run('cd %s && ./run bower_install' % env.root_directory)


def touch_wsgi():
    if env.host:
        print 'Touching apache wsgi file'
        _run('touch %s' % path.join(env.apache_directory, 'main', 'apache.wsgi'))

# utilities 
class _VersionControl:
    def __init__(self, settings):
        try:
            self.type = settings.VERSION_CONTROL['type']
        except AttributeError:
            # guess vc
            if path.isdir(path.join(path.dirname(__file__), '.svn')):
                self.type = 'svn'
            elif path.isdir(path.join(path.dirname(__file__), '.git')): 
                self.type = 'git'
            else:
                raise Exception('Cannot determine vc system.')
            #self.type = 'git'

        if self.type=='svn':
            self.svn_repository = env.username
            self.svn_server = 'http://svn.s-cape.cz' 
            self.svn_base = '%s/%s' % (self.svn_server, self.svn_repository)
        elif self.type=='git':
            self.git_repository = env.username
            self.git_server = 'git@s-cape.cz'

    def up(self):
        if self.type=='svn':
            self.svn_up_project()
            self.svn_up_apache()
        elif self.type=='git':
            self.git_pull()
            
    def git_pull(self):
      if _exists(path.join(env.root_directory, '.git')):
        print 'Pulling newest version from git'
        _run('cd %s && git pull' % env.root_directory)
      else:
        print 'Git clone not yet implemented'

    def svn_up_project(self):
        if _exists(env.root_directory):
            print 'Updating project from svn'
            _run('cd %s && svn up' % env.root_directory)
        else:
            print 'Checking out project from svn'
            _run('svn co --username deploy --password `cat /etc/auths/svn_deploy_password` %s/project %s' % (vc.svn_base, env.root_directory))

    def svn_up_apache(self):
        if not env.host: return

        if exists(env.apache_directory):
            print 'Updating apache config from svn'
            _run('cd %s && svn up' % env.apache_directory)
        else:
            print 'Updating apache config from svn'
            _run('svn co %s/apache %s' % (vc.svn_base, env.apache_directory))

vc = _VersionControl(settings)



def _exists(name):
    if env.host_string:
        return fabric_exists(name)
    else:
        return path.exists(name)
 
def _run(*args, **kwargs):
    if env.host:
        fabric_run(*args, **kwargs)
    else:
        local_run(capture=False, *args, **kwargs)

def _virtualenv_run(command):
    _run(env.virtualenv_activate + ' && ' + command)
