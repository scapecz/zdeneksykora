import os
import sys
import site

os.environ['DJANGO_SETTINGS_MODULE'] = 'sykora.settings'

www_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../')

site_packages = os.path.join(www_dir, 'env/lib', 'python%s' % sys.version[:3], 'site-packages')
prev_sys_path = list(sys.path)
site.addsitedir(site_packages)
sys.real_prefix = sys.prefix
sys.prefix = os.path.join(www_dir, 'env')
# Move the added items to the front of the path:
new_sys_path = [www_dir, os.path.join(www_dir, 'project')]
for item in list(sys.path):
    if item not in prev_sys_path:
        new_sys_path.append(item)
        sys.path.remove(item)
sys.path[:0] = new_sys_path

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
